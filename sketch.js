let cam;
let angle = 0;

function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight, WEBGL)
  cam = createCapture(VIDEO);
  cam.size(200,200);
  cam.hide();
}

function draw() {
  background(175);
  rotateX(angle);
  rotateY(angle);
  rotateZ(angle);
  noStroke();
  ambientMaterial(255);
  angle += 0.03;
  texture(cam);
  box(200);
  // put drawing code here
}